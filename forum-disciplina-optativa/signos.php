<?php
    $element = simplexml_load_file("signos.xml") or die("falha ao carregar XML");

    $found = false;
    $nascimentoUsuario = date_create($_POST["data"]);
    $anoFixo = $nascimentoUsuario->format("Y");
    $nasc = $nascimentoUsuario->format("Y-m-d");

    function exibe_signo($s) {
        echo "<center>";
        echo "<h1>Seu signo: $s->signoNome</h1>";
        echo "<h2>Descrição: $s->descricao</h2>";
        echo "<h2>Inicia em $s->dataInicio e termina em $s->dataFim";
        echo "</center>";
    }

    foreach($element->signo as $signo) {

        $di = explode("/", $signo->dataInicio);
        $df = explode("/", $signo->dataFim);

        // cria as datas
        $dataInicioSigno = "$anoFixo-$di[1]-$di[0]";
        if (intval($di[1]) == 12 && intval($df[1]) == 1) {
            $anoFixo = intval($anoFixo) + 1;
        }

        $dataFimSigno = "$anoFixo-$df[1]-$df[0]";

        if(($nasc >= $dataInicioSigno) && ($nasc <= $dataFimSigno)) {
            exibe_signo($signo);
            $found = true;
            break;
        }
    }

    if(!$found) {
        $anoFixo = intval($anoFixo) - 2;
    
        foreach($element->signo as $signo) {

            $di = explode("/", $signo->dataInicio);
            $df = explode("/", $signo->dataFim);
    
            // cria as datas
            $dataInicioSigno = "$anoFixo-$di[1]-$di[0]";
            if (intval($di[1]) == 12 && intval($df[1]) == 1) {
                $anoFixo = intval($anoFixo) + 1;
            }
    
            $dataFimSigno = "$anoFixo-$df[1]-$df[0]";
            
            if(($nasc >= $dataInicioSigno) && ($nasc <= $dataFimSigno)) {
                exibe_signo($signo);
            }
        }
    }
?>
